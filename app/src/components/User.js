import { UsersIcon, StarIcon } from '@heroicons/react/24/outline';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

export default function User({ userInformation }) {
  const { name, avatar_url, followers, following, login, bio, stars } =
    userInformation;

  const navigate = useNavigate();

  useEffect(() => {
    if (!userInformation.stars) return navigate('/');
  }, [navigate, userInformation]);
  return (
    <div className="mx-4 mt-6 mb-6 border-b border-slate-500 pb-4 sm:mx-6 sm:w-1/4">
      {userInformation.stars && (
        <>
          <div className="mb-6 flex flex-auto sm:flex-col sm:justify-center">
            <img
              src={avatar_url}
              alt={name}
              aria-label="user image"
              className="mr-4 w-1/6 rounded-full border border-slate-300 shadow-[0_0_2px_0_rgba(0,0,0,0.5)] shadow-slate-500 sm:w-3/4 sm:flex-auto"
            />
            <div className="flex flex-col justify-center sm:mt-4">
              <h2 className="text-2xl font-semibold">{name}</h2>
              <p className="text-xl font-light">{login}</p>
            </div>
          </div>
          <p className="mb-4 text-base font-light">{bio}</p>
          <div className="mt-2 flex items-center gap-2 align-middle">
            <UsersIcon className="inline h-4 w-4" />
            <span>{followers} followers</span>
            {'.'}
            <span>{following} following</span>
            <span className="inline-flex items-center">
              {' '}
              <StarIcon className="inline h-4 w-4" />
              {stars.length}
            </span>
          </div>
        </>
      )}
    </div>
  );
}
