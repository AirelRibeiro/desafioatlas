import { StarIcon, BookmarkSquareIcon } from '@heroicons/react/24/outline';

export default function Repositories({ repositories }) {
  return (
    <div className="mx-4 mt-2 sm:mt-16 sm:w-9/12 sm:flex-initial sm:border-2 sm:border-x-slate-300 sm:p-10">
      <span className="mr-2 rounded bg-gray-100 px-2.5 py-0.5 text-2xl font-semibold text-gray-800 dark:bg-gray-700 dark:text-gray-300">
        <BookmarkSquareIcon className="inline-flex h-6 w-6 items-center" />
        Repositories
      </span>
      {repositories.map(
        ({
          id,
          name,
          language,
          updated_at,
          description,
          stargazers_count,
          html_url,
        }) => (
          <div
            key={id}
            className="m-2 flex flex-col border-b border-slate-300 py-6 sm:mt-4"
          >
            <h3 className="mb-1 text-xl font-semibold text-[#0349b4]">
              <a href={html_url} target="_blank" rel="noreferrer">
                {name}
              </a>
            </h3>
            <p className="mb-2 pr-6 text-sm font-light text-black">
              {description}
            </p>
            <div className=" mt-2 flex items-center align-middle text-xs">
              <span className="mr-4">{language}</span>
              <span className="mr-4 inline-flex items-center">
                <StarIcon className="inline-flex h-4 w-4 items-center" />
                {stargazers_count}
              </span>
              <span>
                Updated on{' '}
                {new Date(updated_at).toLocaleDateString('en-us', {
                  month: 'short',
                  day: 'numeric',
                })}
              </span>
            </div>
          </div>
        )
      )}
    </div>
  );
}
