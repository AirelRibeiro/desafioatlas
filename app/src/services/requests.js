import axios from 'axios';

export default async function requestData(userName) {
  const requests = [
    `https://api.github.com/users/${userName.toLowerCase()}`,
    `https://api.github.com/users/${userName.toLowerCase()}/repos`,
    `https://api.github.com/users/${userName.toLowerCase()}/starred`,
  ];
  const [user, repos, stars] = await Promise.all(
    requests.map((url) => axios.get(url))
  );
  return [user.data, repos.data, stars.data];
}
