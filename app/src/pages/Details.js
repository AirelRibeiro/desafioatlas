import Repositories from '../components/Repositories';
import User from '../components/User';
import { HomeIcon } from '@heroicons/react/24/outline';
import { useNavigate } from 'react-router-dom';

export default function Details({ userInformation, repositories }) {
  const navigate = useNavigate();
  return (
    <div>
      <header className="flex justify-end rounded-t-xl border border-b-0 border-gray-200 bg-gray-300 pb-2 hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 dark:border-gray-700 dark:bg-gray-800 dark:text-white dark:hover:bg-gray-800 dark:focus:ring-gray-800">
        <HomeIcon
          className="md:w16 mr-3 mt-3 h-10 w-10 sm:mr-4 sm:mt-2 "
          onClick={() => navigate('/')}
        />
      </header>
      <div className="sm:flex">
        <User userInformation={userInformation} />
        <Repositories repositories={repositories} />
      </div>
    </div>
  );
}
