import { useState } from 'react';
import { MagnifyingGlassIcon } from '@heroicons/react/20/solid';
import Alert from '../components/Alert';

export default function Home({ searchFunction }) {
  const [userName, setUserName] = useState('');
  const [invalid, setInvalid] = useState(false);
  const [notFound, setNotFound] = useState(false);

  return (
    <div className="flex h-screen flex-col items-center justify-center gap-1">
      {notFound && (
        <Alert
          message="Usuário não encontrado no GitHub. Verifique se você digitou o nome corretamente."
          closeAlert={() => setNotFound(!notFound)}
        />
      )}
      <div className="sm:w-3/5">
        <h1 className="sm:self-baseline sm:pl-1">
          Busque Repositório no GitHub
        </h1>
      </div>
      {invalid && (
        <Alert
          message="Informe um nome de usuário válido."
          closeAlert={() => setInvalid(!invalid)}
        />
      )}
      <form className="flex w-3/5 flex-col items-center gap-2 sm:flex-row sm:gap-0">
        <input
          type="text"
          className="block w-full rounded-lg border border-[#24292F] bg-gray-50 p-2.5 text-center text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500 sm:flex-auto sm:text-left sm:text-base"
          placeholder="Digite o nome de usuário"
          onChange={({ target: { value } }) => setUserName(value)}
        />
        <button
          type="button"
          className="inline-flex w-32 justify-center rounded-lg bg-[#24292F] px-5 py-2.5 text-center text-base font-medium text-white hover:bg-[#24292F]/90 focus:outline-none focus:ring-4 focus:ring-[#24292F]/50 dark:hover:bg-[#050708]/30 dark:focus:ring-gray-500"
          onClick={() =>
            userName ? searchFunction(userName, setNotFound) : setInvalid(true)
          }
        >
          <MagnifyingGlassIcon className="h-5 w-5" />
          Buscar
        </button>
      </form>
    </div>
  );
}
