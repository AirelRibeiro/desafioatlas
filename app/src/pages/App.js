import { useState } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import requestData from '../services/requests';
import Details from './Details';
import Home from './Home';

export default function App() {
  const [user, setUser] = useState({});
  const [repositories, setRepositories] = useState([]);
  const navigate = useNavigate();
  async function recoverUser(userName, foundFunction) {
    try {
      const [user, repos, stars] = await requestData(userName);
      setUser({ ...user, stars });
      setRepositories(repos);
      navigate('/details');
    } catch (err) {
      foundFunction(true);
    }
  }

  return (
    <div className="h-screen">
      <Routes>
        <Route
          path="/details"
          element={
            <Details userInformation={user} repositories={repositories} />
          }
        />
        <Route path="/" element={<Home searchFunction={recoverUser} />} />
      </Routes>
    </div>
  );
}
