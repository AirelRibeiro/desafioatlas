import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from '../pages/App';
import axios from 'axios';
import { repos, stars, user } from './mocks/user.mock';
import { MemoryRouter } from 'react-router-dom';

jest.mock('axios');

describe('Teste de integração do funcionamento completo da aplicação', () => {
  describe('Testa a página Home', () => {
    beforeEach(() => {
      axios.get
        .mockResolvedValueOnce(user)
        .mockResolvedValueOnce(repos)
        .mockResolvedValueOnce(stars);
    });

    afterEach(() => {
      axios.get.mockRestore();
    });

    it('Verifica se a página possui um input e um botão', () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );

      const input = screen.getByPlaceholderText(/Digite o nome de usuário/);
      const button = screen.getByRole('button');

      expect(input).toBeInTheDocument();
      expect(button).toBeInTheDocument();
    });

    it('Verifica se é possível preencher o input', () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );

      const input = screen.getByPlaceholderText(/Digite o nome de usuário/);
      userEvent.type(input, 'bollito');

      const user = screen.getByDisplayValue(/bollito/i);
      expect(user).toBeInTheDocument();
    });

    it('Verifica se, clicando no botão com o input vazio, é renderizado um alerta', () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );

      const button = screen.getByRole('button');
      userEvent.click(button);

      const alert = screen.getByText(/Informe um nome de usuário válido./i);
      expect(alert).toBeInTheDocument();
    });

    it('Verifica se, preenchendo corretamente, é possível acessar detalhes de uma pessoa usuária', async () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );

      const input = screen.getByPlaceholderText(/Digite o nome de usuário/);
      const button = screen.getByRole('button');

      userEvent.type(input, 'bollito');
      userEvent.click(button);

      const detailsTitle = await screen.findByText(/Repositories/i);
      expect(detailsTitle).toBeInTheDocument();
    });
  });

  describe('Testa a página Details', () => {
    beforeEach(() => {
      axios.get
        .mockResolvedValueOnce(user)
        .mockResolvedValueOnce(repos)
        .mockResolvedValueOnce(stars);
    });

    afterEach(() => {
      axios.get.mockRestore();
    });

    it('Verifica se a página renderiza as informações da pessoa usuária', async () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );
      const input = screen.getByPlaceholderText(/Digite o nome de usuário/);
      const button = screen.getByRole('button');
      userEvent.type(input, 'bollito');
      userEvent.click(button);

      const img = await screen.findByRole('img', { name: 'user image' });
      const name = await screen.findByRole('heading', { level: 2 });
      const userLogin = await screen.findByText(/bollito/);
      const bio = await screen.findByText(
        /Full Stack Developer - Java | Spring | JavaScript | React/
      );
      const followers = await screen.findByText(/100 followers/);
      const following = await screen.findByText(/200 following/);

      expect(img).toHaveProperty('src', user.data.avatar_url);
      expect(img).toHaveProperty('alt', 'Senhor Bolinho');
      expect(name).toHaveTextContent('Senhor Bolinho');
      expect(userLogin).toBeInTheDocument();
      expect(bio).toBeInTheDocument();
      expect(followers).toBeInTheDocument();
      expect(following).toBeInTheDocument();
    });

    it('Verifica se a página renderiza as informações dos repositórios', async () => {
      render(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>
      );
      const input = screen.getByPlaceholderText(/Digite o nome de usuário/);
      const button = screen.getByRole('button');
      userEvent.type(input, 'bollito');
      userEvent.click(button);

      repos.data.forEach(
        async ({ name, language, stargazers_count, html_url, description }) => {
          expect(await screen.findByText(name)).toBeInTheDocument();
          expect(await screen.findByText(language)).toBeInTheDocument();
          expect(await screen.findByText(stargazers_count)).toBeInTheDocument();
          expect(await screen.findByText(description)).toBeInTheDocument();
          expect(
            await screen.findByRole('link', { name: html_url })
          ).toBeInTheDocument();
        }
      );
      expect(await screen.findByText(/Aug 25/)).toBeInTheDocument();
      expect(await screen.findByText(/Sep 30/)).toBeInTheDocument();
      expect(await screen.findByText(/Oct 5/)).toBeInTheDocument();
      expect(await screen.findByText(/Nov 25/)).toBeInTheDocument();
      expect(await screen.findByText(/Dec 29/)).toBeInTheDocument();
    });
  });
});
