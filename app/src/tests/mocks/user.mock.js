export const user = {
  data: {
    avatar_url:
      'https://images.freeimages.com/vhq/images/istock/previews/5136/51366574-human-drawing.jpg',
    bio: 'Full Stack Developer - Java | Spring | JavaScript | React',
    followers: 100,
    following: 200,
    name: 'Senhor Bolinho',
    login: 'bollito',
  },
};

export const repos = {
  data: [
    {
      id: 1,
      name: 'my_first_project',
      language: 'JavaScript',
      updated_at: '2019-08-25T14:40:17Z',
      stargazers_count: 1,
      html_url: 'https://github.com/bollito/my_first_project',
      description: '',
    },
    {
      id: 2,
      name: 'my_second_project',
      language: 'JavaScript',
      updated_at: '2019-09-30T14:40:17Z',
      stargazers_count: 0,
      html_url: 'https://github.com/bollito/my_second_project',
      description: '',
    },
    {
      id: 4,
      name: 'my_fourth_project',
      language: 'Java',
      updated_at: '2019-11-25T14:40:17Z',
      stargazers_count: 1,
      html_url: 'https://github.com/bollito/my_fourth_project',
      description: '',
    },
    {
      id: 3,
      name: 'other_project',
      language: 'Java',
      updated_at: '2019-10-05T14:40:17Z',
      stargazers_count: 1,
      html_url: 'https://github.com/bollito/other_project',
      description: '',
    },
    {
      id: 5,
      name: 'umpteenth_project',
      language: 'PHP',
      updated_at: '2019-12-29T14:40:17Z',
      stargazers_count: 1,
      html_url: 'https://github.com/bollito/umpteenth_project',
      description: '',
    },
  ],
};

export const stars = {
  data: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
};
