 <h1 align="center">Desafio Frontend React.js Atlas</h1>
 
![image](https://user-images.githubusercontent.com/98190806/206622469-1920245c-05d1-4f78-ba9b-b86fa00753e5.png)

<h2>
  <a href="https://desafioatlas-eta.vercel.app/" target="_blank" rel="noreferrer">
    Clique aqui para acessar a aplicação!
  </a>
</h2>



<details>
  <summary><h2>Mais informações</h2></summary>

<h3 align="justify">
 Objetivo:
</h3>

<p align="justify">
Desenvolver uma aplicação React que mostre o perfil de pessoas desenvolvedoras com conta no GitHub, listando seus repositórios.
</p>

##

<h3 align="justify">
 Ferramentas utilizadas:
</h3>

<div align="left">
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" height="40" alt="JavaScript logo"  />
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg" height="40" alt="React logo"  />
<img src="https://user-images.githubusercontent.com/98190806/192314549-485cf014-f26d-4532-8ef7-1b82c3792cc9.png" height="40" alt="React router logo"  />
<img src="https://user-images.githubusercontent.com/98190806/206619882-f04fa6ac-0ff7-42e0-9885-13bde42b555f.png" height="40" alt="tailwind logo"  />
<img src="https://user-images.githubusercontent.com/98190806/206620073-21507d6e-0227-4bc3-83ea-24e214e13f74.png" height="40" alt="axios logo"  />
<img src="https://user-images.githubusercontent.com/98190806/206620234-c850d928-a58e-4daf-b482-ada5c6a8b1d1.png" height="40" alt="RTL logo"  />
<img src="https://user-images.githubusercontent.com/98190806/206620395-3b303b21-166a-4a9b-9934-c37c6b5f282d.png" height="40" alt="Jest logo"  />
<img src="https://user-images.githubusercontent.com/98190806/206624664-e0851ca7-ac8d-4e48-a8bb-0e05ff88baa3.png" height="40" alt="Vercel logo"  />
</div>

##

<h3 align="justify">
 Detalhes da aplicação:
</h3>

<p align="justify">
A aplicação foi criada com o <a href="https://create-react-app.dev/" target="_blank" rel="noreferrer"><strong>Create React App</strong></a>, tendo seu desenvolvimento orientado ao comportamento. Ela possui duas rotas: <strong>/</strong>, que renderiza Home, permitindo à pessoa usuária buscar qualquer usuário do GitHub pelo username; e <strong>/details</strong>, que renderiza Details, exibindo os detalhes do usuário pesquisado, suas informações, avatar e uma lista dos repositórios públicos. A implementação de rotas foi possibilitada pelo <a href="https://reactrouter.com/en/main" target="_blank" rel="noreferrer"><strong>React-router-dom</strong></a>, enquanto as requisições foram construídas usando <a href="https://axios-http.com/" target="_blank" rel="noreferrer"><strong>Axios</strong></a>.
</p>
<p align="justify">
Priorizando a responsividade, a implementação de estilo seguiu o mobile first, utilizando o <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer"><strong>Tailwind</strong></a>. Os testes foram a última parte da aplicação a serem desenvolvidos, sendo esses testes de integração, escritos com auxílio da <a href="https://testing-library.com/docs/react-testing-library/intro/" target="_blank" rel="noreferrer"><strong>React Testing Library</strong></a> e do <a href="https://jestjs.io/pt-BR/" target="_blank" rel="noreferrer"><strong>Jest</strong></a>. Por fim, o deploy da aplicação foi feito pelo <a href="https://vercel.com/docs" target="_blank" rel="noreferrer"><strong>Vercel</strong></a>.
</p>

##

<h3 align="justify">
 Aprendizados mais interessantes:
</h3>

<p align="justify">
O aprendizado mais singular foi a estilização utilizando Tailwind, já que, apesar de conhecê-lo, foi a primeira vez que desenvolvi uma aplicação usando-o. Mas foi ótimo, é um framework muito prático, as possibilidades dele são bem abrangentes, com combinações de layout bastante intuitivas. Me permitiu construir um desing resposivo com muito mais tranquilidade e nem aprendi metade das possibilidades.
</p>
<p align="justify">
Além disso, pude descobrir a cerca de uma particularidade do Axios em testes com o Jest, por meio de um erro na execução dos testes de integração. Aparentemente, como o Axios é criado para clientes Web, com a atualização a partir da versão 1.0.0 usando ES e o Jest executando o código com Node, ele não roda mais no ambiente de testes sem uma "adaptação". É um issue ainda não completamente corrigdo, mas trabalhável, foi bem legal descobrir, estou um pouco mais preparade para desenvolver testes no frontend.
</p>

##

</details>

<details>
  <summary><h2>Rodando localmente</h2></summary>
  
1. Clone o projeto

```
  git clone git@gitlab.com:AirelRibeiro/desafioatlas.git
```

2. Acesse o diretório da aplicação

```
  cd desafioatlas/app
```

3. Instale as dependências

```
  npm install
```

4. Agora é só iniciar a aplicação

```
  npm start
```

5. Você poderá acessar localmente na porta 3000 do seu localhost

```
  http://localhost:3000/
```

_Caso a porta 3000 esteja ocupada, seu terminal indicará a porta usada para a aplicação_ 😉

  </details>

<details>
  <summary><h2>Rodando os testes</h2></summary>
  
  _Caso já tenha clonado o projeto, pode ir direto para o passo 4._
  
 1. Clone o projeto

```
  git clone git@gitlab.com:AirelRibeiro/desafioatlas.git
```

2. Acesse o diretório da aplicação

```
  cd desafioatlas/app
```

3. Instale as dependências

```
  npm install
```

4. Agora é só executar o script de teste

```
  npm test
```

  </details>
